#include <OneWire.h>
#include <DallasTemperature.h>
#include "WiFi.h"
#include <WebServer.h>

// ----- Début define des pins -----
// Pin du capteur de température
#define ONE_WIRE_BUS 4
// Pin de led
#define LED_PIN 15
// ----- Fin define des pins ------

// ----- Init du capteur de température -----
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
// ----- Fin init du capteur de température -----

// ----- Init pour serveur web -----
const char* ssid = "Maxime's Galaxy S21 5G";
const char* password = "xxxx";
WebServer server(80);
// ----- Fin init pour serveur web -----


void connexion_wifi(){
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  WiFi.begin(ssid, password);
  Serial.println("");
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi Connecté");
  Serial.println(WiFi.localIP());
}

void init_server_web(){
  server.on("/", HTTP_GET, []() {
    sensors.requestTemperatures();
    float temperature = sensors.getTempCByIndex(0);
    String HTML = "<!DOCTYPE html>\
    <html>\
    <head>\
        <meta charset='utf-8'>\
        <title>Température en temps réel</title>\
        <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH' crossorigin='anonymous'>\
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>\
        <script>\
            function mettreAJourTemperature() {\
                $.get('/temperature', function(data) {\
                    $('#temperature').text(data + ' °C');\
                    if(data > 25){\
                      $('#title_temperature').removeClass('text-success');\
                      $('#title_temperature').addClass('text-danger');\
                    }else{\
                      $('#title_temperature').removeClass('text-danger');\
                      $('#title_temperature').addClass('text-success');\
                    }\
                });\
            }\
            function allumeLed(){\
              $.post('/allumeLed');\
              $('#allumeLed').hide();\
              $('#eteindreLed').show();\
            }\
            function eteindreLed(){\
              $.post('/eteindreLed');\
              $('#eteindreLed').hide();\
              $('#allumeLed').show();\
            }\
            setInterval(mettreAJourTemperature, 5000);\
              $(function() {\
                mettreAJourTemperature();\
              });\
        </script>\
        <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz' crossorigin='anonymous'></script>\
    </head>\
    <body class='text-center'>\
        <img src='https://www.researchgate.net/profile/Rachida-Ait-Abdelouahid/publication/339956476/figure/fig2/AS:1071219315048454@1632409995820/Microcontroller-ESP-WROOM-32.ppm'>\
        <h1 id='title_temperature' class='text-success'>Température : <span id='temperature'></span></h1>\
        <button id='allumeLed' type='button' onclick='allumeLed()' class='btn btn-primary'>Allumer la led</button>\
        <button id='eteindreLed' type='button' onclick='eteindreLed()' class='btn btn-primary' style='display:none;'>Éteindre la led</button>\
    </body>\
    </html>";
    server.send(200, "text/html", HTML);
  });
}

void gestion_routes(){
  server.on("/temperature", HTTP_GET, []() {
    sensors.requestTemperatures();
    float temperature = sensors.getTempCByIndex(0);
    server.send(200, "text/plain", String(temperature));
  });

  server.on("/allumeLed", HTTP_POST, []() {
    digitalWrite(LED_PIN,  HIGH); 
  });

  server.on("/eteindreLed", HTTP_POST, []() {
    digitalWrite(LED_PIN,  LOW); 
  });
}

void setup(void)
{
  // Initialisation des composants
  Serial.begin(9600);
  sensors.begin();
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  connexion_wifi();
  
  init_server_web();

  gestion_routes();

  server.begin();

  Serial.println("HTTP server started");
  delay(100); 
}

void loop(void)
{
  server.handleClient();
  /*delay(2000);

  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); 
  Serial.println("DONE");

  Serial.print("Temperature for the device 1 (index 0) is: ");
  Serial.println(sensors.getTempCByIndex(0));

  if(sensors.getTempCByIndex(0) > 24.6){
    digitalWrite(LED_PIN, HIGH);
  }else{
    digitalWrite(LED_PIN, LOW);
  }*/
}